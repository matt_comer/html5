/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'html5',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV.contentSecurityPolicyHeader = 'Content-Security-Policy-Report-Only';
    ENV.contentSecurityPolicy = {
          'default-src': "'none'",
          'script-src': "'self' 'unsafe-eval'",
          'font-src': "'self' http://fonts.gstatic.com http://netdna.bootstrapcdn.com", // Allow fonts to be loaded from http://fonts.gstatic.com
          'connect-src': "'self' http://html5news.herokuapp.com",
          'img-src': "'self'",
          'style-src': "'self' 'unsafe-inline' http://fonts.googleapis.com http://netdna.bootstrapcdn.com", // Allow inline styles and loaded CSS from http://fonts.googleapis.com
          'media-src': "'self'"
    }
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'auto';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
      ENV.contentSecurityPolicyHeader = 'Content-Security-Policy';
      ENV.contentSecurityPolicy = {
          'default-src': "'none'",
          'script-src': "'self' 'unsafe-eval'",
          'font-src': "'self' http://fonts.gstatic.com http://netdna.bootstrapcdn.com", // Allow fonts to be loaded from http://fonts.gstatic.com
          'connect-src': "'self' http://html5news.herokuapp.com",
          'img-src': "'self'",
          'style-src': "'self' 'unsafe-inline' http://fonts.googleapis.com http://netdna.bootstrapcdn.com", // Allow inline styles and loaded CSS from http://fonts.googleapis.com
          'media-src': "'self'"
      }
  }

  return ENV;
};
