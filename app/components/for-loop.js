import Ember from 'ember';

var ForLoopComponent = Ember.Component.extend({
    render: function(buffer){
        for (var i = 0; i < this.get('times'); i++) {
            this._super(buffer);
        }
    }
});

export default ForLoopComponent;