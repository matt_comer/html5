import Ember from 'ember';

var ApplicationController = Ember.ArrayController.extend({
    needs: ['category'],
    now: function () {
        return moment().format('dddd, MMM DD YYYY');
    }.property(),
    mainSectionClass: function() {
        if (this.get('currentPath') === 'index')
        {
            return 'home';
        }
        else
        {
            return this.get('controllers.category.content.shortName');
        }
    }.property('currentPath', 'controllers.category.content')
});

export default ApplicationController;