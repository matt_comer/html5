import Ember from 'ember';
import { request } from 'ic-ajax';

var CategoriesController = Ember.ObjectController.extend({
    main: [],
    aside: []
});

export default CategoriesController;