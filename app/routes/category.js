import Ember from 'ember';
import { request } from 'ic-ajax';

var CategoryRoute = Ember.Route.extend({

    main: [],
    aside: [],
    model: function(params){
        // TODO deal with missing category / invalid URL
        var category = this.categories.filterBy('shortName', params.name)[0];

        var me = this;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            request('http://html5news.herokuapp.com/category/' + category.id).then(function(response){
                me.controllerFor('category').set('main', response.main);
                me.controllerFor('category').set('aside', response.aside);
                resolve(category);
            });
        });
    }
});

export default CategoryRoute;
