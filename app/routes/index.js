import Ember from 'ember';
import { request } from 'ic-ajax';

var IndexRoute = Ember.Route.extend({
    model: function(){
        return Ember.RSVP.hash({
            bannerMessages: request('http://html5news.herokuapp.com/banners'),
            featured: request('http://html5news.herokuapp.com/articles/featured')
        });
    },
    setupController: function(controller, model) {
        controller.setProperties(model);
    }
});

export default IndexRoute;