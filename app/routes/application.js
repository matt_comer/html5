import Ember from 'ember';

var ApplicationRoute = Ember.Route.extend({
    model: function(){
        return this.categories;
    }
});

export default ApplicationRoute;