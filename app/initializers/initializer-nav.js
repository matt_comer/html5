import { request } from 'ic-ajax';

var test = {
    name: 'Init Navigation',
    initialize: function (container, application) {
        application.deferReadiness();
        request('http://html5news.herokuapp.com/articles/categories').then(function(result) {
            container.register('data:categories', result, {instantiate: false, singleton: true});
            container.injection('route', 'categories', 'data:categories');
            application.advanceReadiness();
        });

    }
};

export default test;
